The script expects for two arguments: `country` and `env`.

When running, the script looks into a directory with a name the same as provided `country` argument. This directory should contain a file `${env}-url` with an url to desired environment and two optional files `common-apps` and `${env}-apps`. The `common-apps` contains app names, ports and urls common for all the environments. The `${env}-apps` is for specific data per env. The structure of both files is the same and looks like this:
```
app-name port|full url to the app (http(s?)://{address}:{port})
```

`app name` and `port` should be separated by one space.

